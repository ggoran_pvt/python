from tkinter import *
import tkinter.messagebox
root = Tk()

def random():
    print("random text")

# create menu
mainMenu = Menu(root)
# set menu for app
root.configure(menu=mainMenu)

# create Submenu
subMenu = Menu(mainMenu)

# add submenu
mainMenu.add_cascade(label="File", menu=subMenu)


# add submenu options
subMenu.add_command(label="Random Function", command=random)
subMenu.add_command(label="New File", command=random)

#add separator
subMenu.add_separator()


subMenu.add_command(label="New File", command=random)

root.mainloop()