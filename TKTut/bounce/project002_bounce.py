import time
from tkinter import *

from bounce.classes.Ball import Ball
from bounce.classes.Paddle import Paddle

#
root = Tk()
root.title("Bounce")
# set windows to not resisable
root.resizable(0, 0)
# place window on top
root.wm_attributes("-topmost", 1)
#
canvas = Canvas(root, width=500, height=500, bd=0, highlightthickness=0)

canvas.pack()
root.update()

#
paddle = Paddle(canvas, "blue")
ball = Ball(canvas, paddle, "red")

while ball.hit_bottom == False:
    ball.draw()
    paddle.draw()
    root.update_idletasks()
    root.update()
    time.sleep(0.01)


root.mainloop()