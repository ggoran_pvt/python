class Paddle:
    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(100,10, 200,20, fill=color)
        self.canvas.move(self.id, 200,450)
        self.x = 0
        self.canvas_width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)

    def draw(self):
        position = self.canvas.coords(self.id)
        # move
        self.canvas.move(self.id, self.x,0)
        if position[0] <= 0:
            self.x = 0
        if position[2] >= self.canvas_width:
            self.x = 0

    def turn_left(self, event):
        self.x = -2

    def turn_right(self,event):
        self.x = 2