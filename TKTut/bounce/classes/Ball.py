import random
class Ball:
    def __init__(self, canvas, paddle, color):
        self.canvas = canvas
        self.paddle = paddle
        self.id = canvas.create_oval(10,10, 25,25, fill=color)
        # move ball
        self.canvas.move(self.id, 245,100)
        #
        start = [-3,-2,-1,0,1,2,3]
        random.shuffle(start)
        #
        self.x = start[0]
        self.y = -1
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()
        self.hit_bottom = False

    def hit_paddle(self, ball_position):
        # if hits paddle
        paddle_position = self.canvas.coords(self.paddle.id)
        if ball_position[2] >= paddle_position[0] and ball_position[0] <= paddle_position[2]:
            if ball_position[3] >= paddle_position[1] and ball_position[3] <= paddle_position[3]:
                return True
            return False

    def draw(self):
        # animate ball to move
        self.canvas.move(self.id, self.x, self.y)
        position_ball = self.canvas.coords(self.id)
        # change position_ball up and down if hit edge
        if position_ball[1] <= 0:
            self.y = 1
        if position_ball[3] >= self.canvas_height:
            self.hit_bottom = True
        # change position_ball left right if hit edge
        if position_ball[0] <= 0:
            self.x = 1
        if position_ball[2] >= self.canvas_width:
            self.x = -1
        # end of hitting edge

        # if hit paddle
        if self.hit_paddle(position_ball) == True:
            self.y = -3