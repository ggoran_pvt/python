from tkinter import *

root = Tk()

def printName():
    print("name")

def printOnClick(event):
    print("printing on click")

button1 = Button(root, text="Print name", command=printName)

button2 = Button(root, text="Print on click")
button2.bind("<Button-1>", printOnClick)

button1.pack()
button2.pack()

root.mainloop()

