from tkinter import *

root = Tk()

def leftClick(event):
    print("Left click")

def rightClick(event):
    print("Right click")

def printOnA(event):
    print("AAAAAAAAAAAAAAAA")

frame = Frame(root, width=300, height=250)

frame.bind("<Button-1>", leftClick)
frame.bind("<Button-3>", rightClick)
frame.bind("<Button-2>", printOnA)

frame.pack()

root.mainloop()