from tkinter import *

# create object ( blank window)
root = Tk()

# define label
theLabel = Label(root, text="this is easy")

# show label in window
theLabel.pack()

# infinite loop for run window -> close on X
root.mainloop()
