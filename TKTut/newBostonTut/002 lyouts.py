from tkinter import *

root = Tk()

labelOne = Label(root, text="One", bg ="red", fg="white")
labelTwo = Label(root, text="Two", bg="green", fg="black")
labelThree = Label(root, text="Three", bg="blue", fg="white")

# normal pack
labelOne.pack()
# fill so it fits size (x)
labelTwo.pack(fill=X)
labelThree.pack(side=LEFT, fill=Y)

root.mainloop()

