from tkinter import *

def doNothing():
    print("madafaka")

root = Tk()

mainMenu = Menu(root)

# configure menu
root.config(menu=mainMenu)

# create submenu
subMenu = Menu(mainMenu)
mainMenu.add_cascade(label="File",menu=subMenu)
subMenu.add_command(label="New...", command=doNothing)
subMenu.add_command(label="Test...", command=doNothing)
# create separator
subMenu.add_separator()
subMenu.add_command(label="Exit", command=root.quit)

# second dropdown same menu
editMenu = Menu(mainMenu)
mainMenu.add_cascade(label="Edit", menu=editMenu)
editMenu.add_command(label="Redo", command=doNothing)

# end of menu

# start of toolbar

toolbar = Frame(root, bg="#DE4508")
insertStuff = Button(toolbar, text="Insert Img", command=doNothing)
printStuff = Button(toolbar, text="Print", command=doNothing)
insertStuff.pack(side=LEFT, padx=2, pady=5)
printStuff.pack(side=LEFT, padx=2, pady=5)

toolbar.pack(side=TOP, fill=X)


# status bar

statusBar = Label(root, text="Status: quo", bd=1, relief=SUNKEN, anchor=W)
statusBar.pack(side=BOTTOM, fill=X)




root.mainloop()