from tkinter import *

root = Tk()

# create labels
label1 = Label(root,text="Name:")
label2 = Label(root,text="Password:")

# input filed
entry1 = Entry(root)
entry2 = Entry(root)

# remember me checkbox
checkbox = Checkbutton(root, text="Keep me logged in")

# organize grid
label1.grid(row=0, column=0, sticky=E)
entry1.grid(row=0, column=1)
label2.grid(row=1, column=0, sticky=E)
entry2.grid(row=1, column=1)
#display checkbox
checkbox.grid(row=2, columnspan=2)

root.mainloop()

