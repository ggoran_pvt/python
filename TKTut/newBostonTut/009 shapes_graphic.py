from tkinter import *

root = Tk()

canvas = Canvas(root, width=200, height=100)
# create lines
blackLine = canvas.create_line(0,0, 200,50)
redLine = canvas.create_line(200,50, 0,100, fill="red")

greenBox = canvas.create_rectangle(25,25, 130,60, fill="green")

# hide and show graphic
canvas.delete(redLine)

canvas.pack()
root.mainloop()