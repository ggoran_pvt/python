from tkinter import *


class Buttonz:
    def __init__(self, master):
        # master = root, main windows
        frame = Frame(master)
        frame.pack()

        self.printButton = Button(frame, text="Print Message", command=self.printMessage)
        self.printButton.pack(side=LEFT)

        self.quitButton = Button(frame, text="Quit", command=frame.quit)
        self.quitButton.pack(side=LEFT)

    def printMessage(self):
        print("Printing message")


root = Tk()

# create object
b = Buttonz(root)

root.mainloop()