from tkinter import *

root = Tk()

#variables
equa = ""

# functions

#press button
def btnPress(num):
    global equa
    equa = equa + str(num)
    equasion.set(equa)

def EqualPress():
    global equa
    total = str(eval(equa))
    equasion.set(total)
    equa = total

def Clear():
    global equa
    equasion.set("Enter your equasion")
    equa = ""

# gui part
equasion = StringVar()
calculation = Label(root, textvariable=equasion)
equasion.set("Enter your equasion")

calculation.grid(columnspan=4)

# buttons
Button0 = Button(root, text="0", command= lambda : btnPress(0))
Button1 = Button(root, text="1", command= lambda : btnPress(1))
Button2 = Button(root, text="2", command= lambda : btnPress(2))
Button3 = Button(root, text="3", command= lambda : btnPress(3))
Button4 = Button(root, text="4", command= lambda : btnPress(4))
Button5 = Button(root, text="5", command= lambda : btnPress(5))
Button6 = Button(root, text="6", command= lambda : btnPress(6))
Button7 = Button(root, text="7", command= lambda : btnPress(7))
Button8 = Button(root, text="8", command= lambda : btnPress(8))
Button9 = Button(root, text="9", command= lambda : btnPress(9))
ButtonPlus = Button(root, text="+", command= lambda : btnPress("+"))
ButtonMinus = Button(root, text="-", command= lambda : btnPress("-"))
ButtonMultiply = Button(root, text="*", command= lambda : btnPress("*"))
ButtonDivide = Button(root, text="/", command= lambda : btnPress("/"))
ButtonEquals = Button(root, text="=", command=EqualPress)
ButtonClear = Button(root, text="C", command=Clear)


# add to grid

Button1.grid(row=1, column=0)
Button2.grid(row=1, column=1)
Button3.grid(row=1, column=2)
ButtonPlus.grid(row=1, column=3)

Button4.grid(row=2, column=0)
Button5.grid(row=2, column=1)
Button6.grid(row=2, column=2)
ButtonMinus.grid(row=2, column=3)

Button7.grid(row=3, column=0)
Button8.grid(row=3, column=1)
Button9.grid(row=3, column=2)
ButtonMultiply.grid(row=3, column=3)

ButtonClear.grid(row=4, column=0)
Button0.grid(row=4, column=1)
ButtonDivide.grid(row=4, column=3)
ButtonEquals.grid(row=4, column=2)



#run
root.mainloop()