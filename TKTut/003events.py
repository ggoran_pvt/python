from tkinter import *

root = Tk()

# function
def leftClick(event):
    print("Left click")
def rightClick(event):
    print("Right Click")
def scroll(event):
    print("scrolling")
def leftKey(event):
    print("Left key")
def rightKey(event):
    print("Right key")

# width height
root.geometry("500x500")

root.bind("<Button-1>", leftClick)
root.bind("<Button-2>", rightClick)
root.bind("<Button-3>", scroll)
root.bind("<Left>", leftKey)
root.bind("<Right>", rightKey)
#run
root.mainloop()