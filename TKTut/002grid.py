from tkinter import *

root = Tk()

# label
label1 = Label(root, text="Name")
label2 = Label(root, text="Password")

# grid layout
label1.grid(row=0, column=0, sticky="E")
label2.grid(row=1, column=0, sticky="E")

# create entry input
entrySpace = Entry(root)
entryPassword = Entry(root)

# display entry with grid layout
entrySpace.grid(row=0, column=1)
entryPassword.grid(row=1, column=1)

cButton = Checkbutton(root, text="remember name")
#display in 2 columns
cButton.grid(columnspan=2)


#run
root.mainloop()