from tkinter import *
import random
import time

root = Tk()
# canvas for objects
canvas = Canvas(root, width=300, height=300)
canvas.pack()
# create things
#canvas.create_rectangle(20,20, 100,270)
#canvas.create_line(0,0, 300,300)
#canvas.create_polygon(0,0, 5,20, 50,90, 100,200, 195, 150, fill="red")


def randomRects(num):
    for i in range(0,num):
        x1 = random.randrange(300)
        y1 = random.randrange(300)
        x2 = x1 + random.randrange(300-x1)
        y2 = y1 + random.randrange(300-y1)
        canvas.create_rectangle(x1,y1, x2,y2)

#randomRects(223)

#canvas.create_arc(10,10,200,80,extent=45, style=ARC)
#canvas.create_arc(10,80,200,180,extent=50, style=ARC)


#animation
canvas.create_polygon(10,10, 10,60, 50,35)

#
for i in range(0,60):
    canvas.move(1,5,0)
    root.update()
    time.sleep(0.1)


# run
root.mainloop()