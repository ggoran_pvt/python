from tkinter import *

root = Tk()

#evaluate
def evaluate(event):
    data = e.get()
    # update with new value
    ans.configure(text="Answer: " + str(eval(data)))

label1 = Label(root, text="Enter your expression")
label1.pack()


e = Entry(root)
# on enterupdate
e.bind("<Return>", evaluate)
e.bind("<KP_Enter>", evaluate)
e.pack()


ans = Label(root)
ans.pack()
# run
root.mainloop()